#include <iostream>
#include <windows.h>
#include <fstream>
#include "gammaramp.h"

BOOL CGammaRamp::SetRGB(HDC hDC, WORD wR, WORD wG, WORD wB)
{
    /*
    Changes the brightness of the entire screen.
    This function may not work properly in some video cards.

    The wBrightness value should be a number between 0 and 255.
    128 = Regular brightness
    above 128 = brighter
    below 128 = darker

    If hDC is NULL, SetBrightness automatically load and release 
    the display device context for you.

    */
    BOOL bReturn = FALSE;
    HDC hGammaDC = hDC;

    //Load the display device context of the entire screen if hDC is NULL.
    if (hDC == NULL)
        hGammaDC = GetDC(NULL);

    if (hGammaDC != NULL)
    {
        //Generate the 256-colors array for the specified wBrightness value.
        WORD GammaArray[3][256];

        for (int iIndex = 0; iIndex < 256; iIndex++)
        {
            int iR = iIndex * (wR + 128);
            int iG = iIndex * (wG + 128);
            int iB = iIndex * (wB + 128);

            if (iR > 65535)
                iR = 65535;
            if (iG > 65535)
                iG = 65535;
            if (iB > 65535)
                iB = 65535;

            GammaArray[0][iIndex] = (WORD)iR;
            GammaArray[1][iIndex] = (WORD)iG;
            GammaArray[2][iIndex] = (WORD)iB;
            
        }

        //Set the GammaArray values into the display device context.
        bReturn = SetDeviceGammaRamp(hGammaDC, GammaArray);
    }

    if (hDC == NULL)
        ReleaseDC(NULL, hGammaDC);

    return bReturn;
}

int WINAPI WinMain(HINSTANCE h, HINSTANCE prev_h, LPSTR args, int argc)
{
    // hacky way to parse cmdline argument rgb value
    const char s[2] = " ";
    char *token = strtok(args, s);
    int r = atoi(token); 
    token = strtok(NULL, s);
    int g = atoi(token);
    token = strtok(NULL, s);
    int b = atoi(token);

    CGammaRamp GammaRamp;

    // set the gamma to the cmdline arg values
    GammaRamp.SetRGB(NULL, r, g, b);

    return 0;
}