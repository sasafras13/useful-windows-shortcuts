Modified original scripts at : http://www.nirsoft.net/vc/change_screen_brightness.html

compile with:

`g++ night_light.cpp gamma_ramp.cpp -o night_light.exe && night_light.exe [R] [G] [B]`

Features:

- Set RGB for whole screen via commandline:

Examples:

# turn on night light
`night_light.exe 255 50 0`

# sets normal brightness levels
`night_light.exe 128 128 128`

# sets maximum brightness levels
`night_light.exe 255 255 255`